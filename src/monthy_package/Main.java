package monthy_package;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Predicate;

import static java.lang.Math.abs;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int nbrPlay = 0, score = 0;

        System.out.println("Combien de fois voulez-vous jouer ? :");
        // use different function here ?
        nbrPlay = interfaceScan();


        //System.out.println(calcFact(nbrPlay));
        for (int i=1;i <= nbrPlay;i++){
            System.out.println("Jouons la partie n°"+ i);
            System.out.println("________________________");
            if(onePartyofMonthyHall()) ++score;
        }

        System.out.println("Le jeu est terminé. Votre score est " + score + " sur " + nbrPlay + ". A bientôt.");
    }


    public static int calcFact(int n){
            if (n == 0)
                return 1;
            else
                return(n * calcFact(n-1));
    }
    public static boolean onePartyofMonthyHall(){
        Random randomGenerator = new Random ();
        int choice = 0, lastDoor = 0, score = 0, goodDoor = randomGenerator.nextInt (2) + 1;
        boolean win = false;
        System.out.println("Choisissez une porte entre 1 et 3 : ");
        choice = enterIntegerValue();
        lastDoor = determineLastDoor(goodDoor, choice);

        System.out.println("Monty ouvre la porte " +  lastDoor + " qui cache une chèvre");
        if(goodDoor != choice)
            System.out.println("Choisissez la porte " + goodDoor + " ou " + choice + " : ");
        else
            System.out.println("Choisissez la porte " + goodDoor + " ou " + abs(goodDoor - lastDoor) + " : ");
        choice = enterIntegerValue();
        win = playMonthy(choice, goodDoor);//1

        return win;
    }

    public static int determineLastDoor(int theGood, int theChoice){
        int last = 1;
        while (last == theChoice || last == theGood)
            ++last;
        return last;
    }

    public static boolean playMonthy(int choice, int good){
        boolean win = false;
        if(choice == good){
            System.out.println("La porte " + choice + " contenait une voiture : vous avez gagné un point.");
            win = true;
        }else {
            System.out.println("La porte " + choice + " contenait une chèvre : vous avez perdu un point.");
        }
        return win;
    }

    public static int enterIntegerValue(){
        int variable=0;
        Predicate<Integer> p = i -> i < 1 || i > 3;

        do {
            variable = interfaceScan();
            if(p.test(variable))
                System.out.println("Numbers must be between 1 to 3. Try again.");
        }while(p.test(variable));
        return variable;
    }

    public static int interfaceScan(){
        Scanner scan = new Scanner(System.in);
        int var = 0;
        boolean isNumeric = false;
        do {
            try {
                var = scan.nextInt();
                isNumeric = true;
            } catch(InputMismatchException ime) {
                //Display Error message
                System.out.println("Invalid character found, Please enter numeric values only !!");
                scan.nextLine();//Advance the scanner
            }
        }while(!isNumeric);
        return var;
    }
}
