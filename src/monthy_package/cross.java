
import java.util.Scanner;

public class cross {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean r = true;
        do {
            int length = 0;
            System.out.println("Voulez vous faire une croix ? (true ou false) :");
            r = s.nextBoolean();
            if(r){
                do {
                    System.out.println("Introduisez un nombre impair compris entre 5 et 15 : ");
                    length = s.nextInt();
                    int half = (length/2) +1;
                    if(length >= 5 && length <= 15 && length % 2 != 0){
                        for(int ligne = 1; ligne <= length; ligne++){
                            for (int colonne = 1; colonne <= length;colonne++){
                                if (colonne == half || (ligne == half))
                                    System.out.print("* ");
                                else
                                    System.out.print("  ");
                            }
                            System.out.println();
                        }
                    }else{
                        System.out.println("Donnez une valeur IMPAIR comprise entre 5 et 15, merci.");
                    }
                }while (length >= 5 && length <= 15);
            }
        }while (r);
    }
}
